const bcrypt = require('bcrypt');
const mongoose = require('mongoose');

const adminUserSchema = mongoose.Schema({
    username: {
        type: String, 
        unique: true, 
        required: true
    },
    email: {
        type: String, 
        unique: true, 
        required: true
    },
    password: {
        type: String, 
        require: true
    },
    brand: {
        type: String, 
        require: true
    },
    vendorType: String,
    admin: {
        type: Boolean, 
        default: false
    }
});

adminUserSchema.pre("save", async function(next){
    try{
        if(!this.isModified('password')){
            next();
        }
        let hashedPassword= await bcrypt.hash(this.password,10);
        this.password=hashedPassword;
        next();
    }catch(err){
        return next(err);
    }
})

adminUserSchema.methods.comparePassword = async function(candidatePassword,next){
    try{
        let isMatched = await bcrypt.compare(candidatePassword,this.password);
        return isMatched;
    }catch(err){
        return next(err);
    }
}

const AdminUser = mongoose.model('adminUser',adminUserSchema);

module.exports = AdminUser;