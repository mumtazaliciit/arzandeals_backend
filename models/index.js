const mongoose = require('mongoose');

mongoose.set('debug',true);
mongoose.Promise=Promise;
mongoose.connect(process.env.MONGODB_URL || 'mongodb://localhost/arzandeals',{keepAlive: true});

module.exports = require('./cloth-add');
module.exports = require('./food-add');