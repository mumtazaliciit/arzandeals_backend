const mongoose = require('mongoose');
var bcrypt = require("bcrypt");


const userSchema = new mongoose.Schema({
    username: {type: String, unique: true, required: true},
    email: {type: String, unique: true, required: true},
    password: {type: String, require: true},
    isVerified: {type: Boolean, default: false},
    passwordResetToken: String,
    passwordResetDate:  Date
});

userSchema.pre('save', async function(next){
    try{
        if(this.isModified('password')){
            return next();
        }
        let hashedPassword= await bcrypt.hash(this.password,10);
        this.password=hashedPassword;
        return next();
    }catch(err){
        return next(err);
    }
});

userSchema.method.comparePassword = async function(candidatePassword,next){
    try{
        let isMatched = await bcrypt.compare(candidatePassword,this.password);
        return isMatched;
    }catch(err){
        return next(err);
    }
}

const User = mongoose.model('user',userSchema);

module.exports = User;