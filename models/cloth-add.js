const mongoose = require('mongoose');

const clothAddSchema = mongoose.Schema({
    brand: {
        type: String,
        require: true
    },
    brandImageUrl: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    discount: {
        type: String,
        required: true
    },
    imageUrl: {
        type: String,
        required: true
    },
    linkUrl: {
        type: String,
        required: true
    },
    details:{
        type: Array,
        'default': []
    },
    tcs:{
        type: Array,
        'default': []
    },
    clickCount: {
        type: Number,
        default: 0
    },
    expiry: String,
    offerType: String,
});

const ClothAdd =  mongoose.model('cloth-add', clothAddSchema);
module.exports = ClothAdd;