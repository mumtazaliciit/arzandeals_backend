const errorHandler= function(error,req,res,next){
    return res.status(error.status || 500).json({
        error: {
            message: error.message || 'Ops, something went wrong'
        }
    });
}

module.exports = errorHandler;