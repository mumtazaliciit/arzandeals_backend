const FoodAdd =require('../models/food-add');

exports.postFoodAdd = async function(req, res, next){
    try{
        let newAdd= await FoodAdd.create(req.body);
        return res.status(200).json(newAdd);
    }catch(err){
        return next(err);
    }
}

exports.getFoodAdds = async function(req, res, next){
    try{
        let foodDeals= await FoodAdd.find();
        return res.status(200).json(foodDeals);
    }catch(err){
        return next(err);
    }
}

exports.getFoodByID = async function(req,res,next){
    try{
        let foodDeal = await FoodAdd.findById(req.params.id);
        return res.status(200).json(foodDeal);
    }catch(err){
        return next(err);
    }
}

exports.deleteFoodAdd = async function(req,res,next){
    try{
        await FoodAdd.findByIdAndRemove(req.params.id);
        return res.status(200).json({operationSucessful: true})
    }catch(err){
        return next(err);
    }
}