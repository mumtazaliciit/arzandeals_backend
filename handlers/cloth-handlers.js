const ClothAdd =require('../models/cloth-add');

exports.postClothAdd = async function(req, res, next){
    try{
        let newAdd= await ClothAdd.create(req.body);
        return res.status(200).json(newAdd);
    }catch(err){
        return next(err);
    }
}

exports.getClothAdds = async function(req, res, next){
    try{
        let clothDeals= await ClothAdd.find();
        return res.status(200).json(clothDeals);
    }catch(err){
        return next(err);
    }
}

exports.getClothByBrand = async function(req, res, next){
    try{
        let clothDeals= await ClothAdd.find({brand: req.params.brand});
        return res.status(200).json(clothDeals);
    }catch(err){
        return next(err);
    }
}
exports.deleteClothAdd = async function(req,res,next){
    try{
        await ClothAdd.findByIdAndRemove(req.params.id);
        return res.status(200).json({operationSucessful: true})
    }catch(err){
        return next(err);
    }
}