const AdminUser = require('../models/admin-user');
const jwt = require('jsonwebtoken');


exports.adminSigUp= async function(req,res,next){
    console.log(req.body);
    if(req.body.passKey!==process.env.CREATE_KEY){
        let errorMessage = 'you do not have permission to create user';
        return next({
            status: 400,
            message: errorMessage
        })
    }else{
        try{
            console.log(req.body.user);
            let adminUser = await AdminUser.create(req.body.user);
            let {id,username}=adminUser;
            let token = jwt.sign({
                id,
                username,
            }, process.env.SECRET_KEY);
            return res.status(200).json({
                message: 'admin user created',
                token       
            })
        }catch(err){
            if(err.code===11000)
            err.message = 'brand /  email / username already exist';
            return next({
                status: 400,
                message: err.message
            });
        }
    }
}

exports.adminSignIn = async function(req,res,next){
    try{
        let user = await AdminUser.findOne({username: req.body.username});
        if(user){
            let {id, brand, username, vendorType, admin} = user;
            let isMatched = await user.comparePassword(req.body.password);
            if(isMatched){
                let token = jwt.sign({
                    id,
                    username,
                    brand,
                    vendorType,
                    admin
                }, process.env.SECRET_KEY);
                return res.status(200).json({
                    id,
                    username,
                    brand,
                    vendorType,
                    admin,
                    token
                });
            }else{
                return next({
                    status: 400,
                    message: 'invalid username / password'
                })
            }
        }else{
            return next({
                status: 400,
                message: 'invalid username / password'
            })
        }
    }catch(err){
        return next(err);
    }
}