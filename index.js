require('dotenv').config();
const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser =require("body-parser");
const db = require('./models')
const clothSaleRoutes =require('./routes/cloth-sale');
const foodDealsRoutes =require('./routes/food-deals');
const authRoutes = require('./routes/auth-routes')
const errorHandler =require('./handlers/errors');

const PORT = process.env.PORT || 8081;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.use(express.json());

app.use('/api/clothsale', clothSaleRoutes);
app.use('/api/fooddeals', foodDealsRoutes);
app.use('/api/auth', authRoutes)

app.use(function(req, res, next) {
  let err = new Error("Not Found");
  err.status = 404;
  next(err);
});

app.use(errorHandler);

app.listen(PORT, function() {
  console.log(`Server is starting on port ${PORT}`);
});
