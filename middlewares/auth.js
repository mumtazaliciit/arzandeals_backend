const jwt = require('jsonwebtoken');
const AdminUser = require('../models/admin-user');

exports.isLoggedIn = function(req,res,next){
    try{
        console.log(req.get('Authorization'));
        const token=req.get('Authorization').split(' ')[1];
        jwt.verify(token,process.env.SECRET_KEY,function(err,decode){
            if(decode){
                console.log('helo 1');
                next();
            }else{
                next({
                    status: 401,
                    message: 'you need to be loggoed in, please'
                });
            }
        });
    }catch(err){
        return next({
            status: 401,
            message: 'you need to be loggoed in, please'
        });
    }
}

exports.ensureCorrectAdminUser = async function(req,res,next){
    try{
        const token = req.get('Authorization').split(' ')[1];
        await jwt.verify(token, process.env.SECRET_KEY, async function(err,decode){
            if(decode){
                const user = await AdminUser.findById(decode.id);
                if(user.admin){
                    console.log('helo 2');
                    next();
                }else if(user.brand===req.params.brand || user.brand===req.body.brand){
                    next();
                }else{
                    next({
                        status: 401,
                        message: 'you are not authorized for this action!'
                    });
                }
            }
        });
    }catch(err){
        next({
            status: 500,
            message: 'something went wrong!'
        })
    }
}