const express = require('express');
const Router = express.Router();
const clothHandlers =require('../handlers/cloth-handlers');
const { isLoggedIn, ensureCorrectAdminUser } = require('../middlewares/auth');

Router.get('/', clothHandlers.getClothAdds);
Router.post('/new', isLoggedIn, ensureCorrectAdminUser, clothHandlers.postClothAdd);
Router.get('/:brand',clothHandlers.getClothByBrand);
Router.delete('/:brand/:id', isLoggedIn, ensureCorrectAdminUser, clothHandlers.deleteClothAdd);

module.exports = Router;