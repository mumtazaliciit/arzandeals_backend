const express = require('express');
const Router = express.Router();
const authHandlers =require('../handlers/authHandlers');

Router.post('/admin/secret/hamonoptra',authHandlers.adminSigUp);
Router.post('/admin/signin',authHandlers.adminSignIn);

module.exports = Router;