const express = require('express');
const Router = express.Router();
const foodHandlers =require('../handlers/food-handlers');
const { isLoggedIn, ensureCorrectAdminUser } = require('../middlewares/auth');

Router.get('/', foodHandlers.getFoodAdds);
Router.post('/new', isLoggedIn, ensureCorrectAdminUser, foodHandlers.postFoodAdd);
Router.get('/:id',foodHandlers.getFoodByID);
Router.delete('/:brand/:id',isLoggedIn, ensureCorrectAdminUser, foodHandlers.deleteFoodAdd);

module.exports = Router;